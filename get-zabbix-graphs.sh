#!/bin/bash
#
# Zabbix get graph by djarcangel@djarcangel.com
# Get's monthly stat graphs from zabbix
#
# usage: ./get-zabbix-graph <servername> 

### VARIABLE ###

# Temp dir to store graphs
DIR=/tmp

# Desired graph image size
WIDTH="600"
HEIGHT="400"

# The period is automatically from $PERIOD to now
PERIOD="now-30d"

# This is either your.zabbix.host or your.zabbix.host/zabbix
ZABBIXURL="your.zabbix.host"

# Mysql Password
MYSQLPASS=""

### VARIABLE END HERE ###

# Host parameter
HOST="$1"

# Get the ID for hostname
HOSTID=`mysql -u root -p$MYSQLPASS zabbix -e "select hostid from hosts where host='$HOST';" | tail -n 1 | head -n 1`


# Get the graph ID's for host
CPULOAD=`mysql -u root -p$MYSQLPASS zabbix -e "SELECT DISTINCT g.graphid,g.name FROM graphs g,graphs_items gi,items i WHERE i.hostid='$HOSTID' AND gi.graphid=g.graphid AND i.itemid=gi.itemid AND (g.name IN ('CPU load','CPU utilization','Memory usage','Disk space usage graph /')) AND g.flags IN (0,4);" | head -n 2 | tail -n 1 | awk '{print $1}'`

CPUUTIL=`mysql -u root -p$MYSQLPASS zabbix -e "SELECT DISTINCT g.graphid,g.name FROM graphs g,graphs_items gi,items i WHERE i.hostid='$HOSTID' AND gi.graphid=g.graphid AND i.itemid=gi.itemid AND (g.name IN ('CPU load','CPU utilization','Memory usage','Disk space usage graph /')) AND g.flags IN (0,4);" | head -n 3 | tail -n 1 | awk '{print $1}'`

DISK=`mysql -u root -p$MYSQLPASS zabbix -e "SELECT DISTINCT g.graphid,g.name FROM graphs g,graphs_items gi,items i WHERE i.hostid='$HOSTID' AND gi.graphid=g.graphid AND i.itemid=gi.itemid AND (g.name IN ('CPU load','CPU utilization','Memory usage','Disk space usage graph /')) AND g.flags IN (0,4);" | head -n 4 | tail -n 1 | awk '{print $1}'`

MEM=`mysql -u root -p$MYSQLPASS zabbix -e "SELECT DISTINCT g.graphid,g.name FROM graphs g,graphs_items gi,items i WHERE i.hostid='$HOSTID' AND gi.graphid=g.graphid AND i.itemid=gi.itemid AND (g.name IN ('CPU load','CPU utilization','Memory usage','Disk space usage graph /')) AND g.flags IN (0,4);" | head -n 5 | tail -n 1 | awk '{print $1}'`


# remove dir because we always want latest graphs
rm -rf $DIR/$1
# create the temp dir
mkdir $DIR/$1

# Get the actual graphs
wget --quiet -O $DIR/$1/cpuload.png "https://$ZABBIXURL/chart2.php?graphid=$CPULOAD&from=$PERIOD&to=now&profileIdx=web.graphs.filter&profileIdx2=$HEIGHT&width=$WIDTH"
wget --quiet -O $DIR/$1/cpuutil.png "https://$ZABBIXURL/chart2.php?graphid=$CPUUTIL&from=$PERIOD&to=now&profileIdx=web.graphs.filter&profileIdx2=$HEIGHT&width=$WIDTH"
wget --quiet -O $DIR/$1/diskspace.png "https://$ZABBIXURL/chart2.php?graphid=$DISK&from=$PERIOD&to=now&profileIdx=web.graphs.filter&profileIdx2=$HEIGHT&width=$WIDTH"
wget --quiet -O $DIR/$1/memory.png "https://$ZABBIXURL/chart2.php?graphid=$MEM&from=$PERIOD&to=now&profileIdx=web.graphs.filter&profileIdx2=$HEIGHT&width=$WIDTH"